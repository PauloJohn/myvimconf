"-------------------------Vundle Configurations----------------------------------
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" My plugins
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdtree'
Plugin 'kien/ctrlp.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes' 
Plugin 'powerline/powerline' 
Plugin 'w0rp/ale'
Plugin 'tpope/vim-surround' 
Plugin 'jiangmiao/auto-pairs' 
Plugin 'alvan/vim-closetag' 
Plugin 'Valloric/YouCompleteMe'
Plugin 'scrooloose/nerdcommenter'


" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

"------------------------General Configurations----------------------------------

" Column delimiter 
highlight ColorColumn ctermbg=darkgray
set colorcolumn=80

" searching
set ignorecase " case insensitive searching
set smartcase " case-sensitive if expresson contains a capital letter
set incsearch " set incremental search, like modern browsers
set hlsearch " set highlighted searches
set nolazyredraw " don't redraw while executing macros

set magic " Set magic on, for regex

set showmatch " show matching braces
set mat=2 " how many tenths of a second to blink

" switch syntax highlighting on
syntax on

set encoding=utf8
let base16colorspace=256  " Access colors present in 256 colorspace"
set t_Co=256 " Explicitly tell vim that the terminal supports 256 colors"

" indentation
set autoindent " automatically set indent of new line
set smartindent
set nowrap
set linebreak

set laststatus=2 " show the satus line all the time

" softtab
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab

" turn off swapfiles
set noswapfile
set nobackup
set nowb

" persistent undo 
if has('persistent_undo') && !isdirectory(expand('~').'/.vim/backups')
silent !mkdir ~/.vim/backups > /dev/null 2>&1
set undodir=~/.vim/backups
set undofile
endif

" paste mode
set pastetoggle=<leader>p

" line numbers
set number

" window pane resizing
nnoremap <silent> <Leader>[ :exe "resize " . (winheight(0) * 3/2)<CR>
nnoremap <silent> <Leader>] :exe "resize " . (winheight(0) * 2/3)<CR>

"------------------------Invisible Char configurations---------------------------
" Shortcut to rapidly toggle `set list`
nmap <leader>l :set list!<CR>

" Use the same symbols as TextMate for tabstops and EOLs
set listchars=tab:▸\ ,eol:¬,trail:~

"-------------------------Mappings-----------------------------------------------
"Nerdtree key
nmap <C-\> :NERDTreeToggle<CR>

"Buffer navegation
nmap <leader>t :enew <CR>
nmap <leader>j :bprevious <CR>
nmap <leader>k :bn <CR>
nmap <leader>x :bdelete <CR>
"change buffers without saving
set hidden


"-------------------------Plugin settings----------------------------------------

"vim-airline"
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"
let g:airline_theme = 'solarized'
let g:airline_solarized_bg='dark'

" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1

" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

"vim-closetag
let g:closetag_filenames = '*.html,*.xhtml,*.phtml,*.html.erb'


"ruby block
runtime macros/matchit.vim
if has("autocmd")
    filetype indent plugin on
endif

"-------------------------Crlp Configurations---------------------------------
" Setup some default ignores
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.(git|hg|svn)|\_site)$',
  \ 'file': '\v\.(exe|so|dll|class|png|jpg|jpeg)$',
\}

" Use the nearest .git directory as the cwd
" This makes a lot of sense if you are working on a project that is in version
" control. It also supports works with .svn, .hg, .bzr.
let g:ctrlp_working_path_mode = 'r'

" Easy bindings for its various modes
nmap <leader>bb :CtrlPBuffer<cr>
nmap <leader>bm :CtrlPMixed<cr>
nmap <leader>bs :CtrlPMRU<cr>
"-------------------------Python Configurations---------------------------------

"code-formatter
autocmd FileType python nnoremap <LocalLeader>= :0,$!yapf<CR>

"sort-import
autocmd FileType python nnoremap <LocalLeader>i :!isort %<CR><CR>

" Run python code
autocmd FileType python nnoremap <LocalLeader>r :w !python<CR>



" Insert debbuger
map <Leader>d :call InsertLine()<CR>

function! InsertLine()
  let trace = expand("import ipdb; ipdb.set_trace()")
  execute "normal o".trace
endfunction

"-------------------------Seeing Is Bealiving------------------------------------

" Annotate every line
  nmap <leader>b :%!seeing_is_believing --timeout 12 --line-length 500 --number-of-captures 300 --alignment-strategy chunk<CR>;
" Annotate marked lines
  nmap <leader>n :%.!seeing_is_believing --timeout 12 --line-length 500 --number-of-captures 300 --alignment-strategy chunk --xmpfilter-style<CR>;
" Remove annotations
  nmap <leader>c :%.!seeing_is_believing --clean<CR>;
" Mark the current line for annotation
  nmap <leader>m A # => <Esc>
" Mark the highlighted lines for annotation
  vmap <leader>m :norm A # => <Esc>

# Requirements for vim configuration:

## Powerline fonts
```
sudo apt-get install fonts-powerline
```

## Isort:
```
pip install isort
```

## YouCompleteMe
```
sudo apt-get install python-dev
sudo apt-get install vim-nox
```

## Python Debbuging
```
pip install ipdb
```


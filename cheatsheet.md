- discart changes and reload the file
```
:edit!
```

- How to reload files with ctrlp
```
<F5>
```

- How to reload files for nerdtree
```
r
```

- Open a file in a new tab
```
:n newfile.txt
```

- Open a file in a previous tab
```
prev newfile.txt
```

- Recover fromm Ctrl+s
```
Ctrl+q
```

- Global search
```
grep -R '.ad' .
```

- Global search limiting the kind of files
```
grep '.ad' **/*.css
```

- Global search for strings instead of regex
```
grep -F '.ad' **/*.css
```

-Disable highlight search until next search
```
:nohlsearch
```
- Creating a newtab
```
:tabnew testing123.py
```
- Reload vimrc
```
:source ~/.vimrc
```

- Add a new buffer without open it
```
:badd file.txt
```
 
### Open files nerdtree
- open file in a new buffer
```
go 
```

- splits verticaly
```
gs
```

- splits horizontaly
```
gi
```

### Ctrp navegate through modes
```
Press <c-f> and <c-b> to cycle between modes.
```
